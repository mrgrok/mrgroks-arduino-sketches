#ifndef Button_h
#define Button_h

class Button
{
  public: 
    Button(short pin);
    void OnChange(void (*onChange)(bool, bool));
    bool Update();
    bool GetState();

  private:
    short _pin;
    bool _state;
    bool _oldState;
    void (*_onChange)(bool, bool);
    static void OnChangeIgnore(bool, bool);
};
#endif
