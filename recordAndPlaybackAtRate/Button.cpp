#include "Button.h"
#include "Arduino.h"

Button::Button(short pin) {
  _pin = pin;
  pinMode(_pin, INPUT);
  _onChange = &OnChangeIgnore;
}

void Button::OnChangeIgnore(bool state, bool oldState) {}

void Button::OnChange(void (*onChange)(bool, bool)) {
  _onChange = onChange;
}

bool Button::Update() {
  _oldState = _state;
  
  bool state = digitalRead(_pin);
  _state = state;

  bool stateChanged = (state != _oldState);
  if(stateChanged)
  {
    _onChange(_state, _oldState);
  }

  return state;
}

bool Button::GetState() {
  return _state;
}
