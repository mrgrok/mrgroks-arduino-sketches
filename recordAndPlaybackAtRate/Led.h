#ifndef Led_h
#define Led_h

class Led
{
  public: 
    Led(short pin);
    void set(bool state);

  private:
    short _pin;
};
#endif
