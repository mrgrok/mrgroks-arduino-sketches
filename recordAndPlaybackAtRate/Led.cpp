#include "Led.h"
#include "Arduino.h"

Led::Led(short pin) {
  _pin = pin;
  pinMode(_pin, OUTPUT);
}

void Led::set(bool state) {
  digitalWrite(_pin, state);
}
