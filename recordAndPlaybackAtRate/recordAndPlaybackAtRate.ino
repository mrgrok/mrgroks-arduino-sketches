#include "Button.h"
#include "Led.h"
#include "LoopMode.h"

const byte maxSamples = 100;

// buffers to record state/duration combinations
boolean states[maxSamples];
int durations[maxSamples];
int currentSampleCycles;
int sampleLength = 0;

short idxBuffer = 0;
LoopMode loopMode;

Led led(9);
Button morseBtn(4);
Button modeBtn(6);

void setup() {
  Serial.begin(9600);
  morseBtn.OnChange(morseBtnChanged);
  modeBtn.OnChange(modeBtnChanged);
}

void loop() {
  checkForInput();
  
  if(loopMode == recording)
  {
    recordCycle(morseBtn.GetState());
  }
  else if(loopMode == playing)
  {
    playbackCycle();
  }
}

void checkForInput() {
  morseBtn.Update();
  modeBtn.Update();
}

void recordCycle(bool morseState)
{
  durations[idxBuffer] = sampleLength;

  if (states[idxBuffer] != morseState)
  {
    // state changed, go to next idx and reset duration   
    idxBuffer++;
    if(idxBuffer == maxSamples) { idxBuffer = 0; } // reset idx if max array size reached
    states[idxBuffer] = morseState;
    
    sampleLength = 0;
    durations[idxBuffer] = sampleLength;
  }
  
  sampleLength++;
  Serial.println(sampleLength);
}

void playbackCycle()
{  
  // play the loop back at the desired speed
  if(currentSampleCycles == 0) {
    // state changed
    updateLed(states[idxBuffer]);
  }
  
  if(idxBuffer == maxSamples) { 
    // EOF recorded loop - repeat
    idxBuffer=0; 
    currentSampleCycles=0;
    Serial.println("BEGIN AGAIN");
  } else {
    Serial.println(currentSampleCycles);
    if(currentSampleCycles < durations[idxBuffer]) {
      // still in same sample, but in next cycle (==sampleLength approx)
      currentSampleCycles++;
    } else {
      // EOF current sample in recorder buffer 
      idxBuffer++; // move to next sample 
      currentSampleCycles = 0; // reset for next sample
    }
  }
}

void startRecording() {
  updateLed(LOW);
  
  // reset record buffers
  memset(states, 0, sizeof(states));
  memset(durations, 0, sizeof(durations));
  
  // reset buffer pointer and sample length
  idxBuffer = 0;
  sampleLength = 0;
  
  loopMode = recording;
}

void startPlayback() {
  updateLed(LOW);
  
  // reset buffer pointer
  idxBuffer=0; 
  
  // 
  currentSampleCycles=0;
  
  loopMode = playing;
}

void stopPlayback() {
  updateLed(LOW);
  loopMode = stopped;
}

void updateLed(bool state) {
  led.set(state);
}

void morseBtnChanged(bool state, bool oldState) {
  if(loopMode == recording) {
    updateLed(state);
  }
  else if(state)
  {
    if(loopMode == playing) {
      stopPlayback();
    } else {
      startPlayback();
    }
  }
}

void modeBtnChanged(bool state, bool oldState) {
  if(state != oldState) {
    // change mode
    if(loopMode != recording) {
      startRecording();
    } 
    else 
    {
      startPlayback();
    }
  }
}

